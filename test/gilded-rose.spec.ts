import { expect } from 'chai';
import { GildedRose } from '../app/gilded-rose';
import { GildedRoseItem, GildedRoseItemName } from '../app/gilded-rose/models';

describe('Gilded Rose', function () {

    it('Add new item', function () {
        const gildedRose = new GildedRose([new GildedRoseItem({ name: 'foo', sellIn: 1, quality: 50 })]);
        const [normalItem] = gildedRose.updateQuality();
        expect(normalItem.name).to.equal('foo');
    });
});

describe('Update Quality, Normal Items', () => {

    it('Should update Quality and SellIn for 1 day', () => {
        const gildedRose = new GildedRose([new GildedRoseItem({ name: 'Normal Item', sellIn: 2, quality: 4 })]);
        const [normalItem] = gildedRose.updateQuality();
        expect(normalItem.quality).to.equal(3);
        expect(normalItem.sellIn).to.equal(1);
    });

    it('Quality degrades twice as fast for sellin zero days', () => {
        const gildedRose = new GildedRose([new GildedRoseItem({ name: 'Normal Item', sellIn: -1, quality: 4 })]);
        const [normalItem] = gildedRose.updateQuality();
        expect(normalItem.quality).to.equal(2);
        expect(normalItem.sellIn).to.equal(-2);
    });

    it('The Quality of an item is never negative', () => {
        const gildedRose = new GildedRose([new GildedRoseItem({ name: 'Normal Item', sellIn: 0, quality: 1 })]);
        const [normalItem] = gildedRose.updateQuality();
        expect(normalItem.quality).to.equal(0);
        expect(normalItem.sellIn).to.equal(-1);
    });

    it.skip('The Quality of an item is never more than 50', () => {
        const gildedRose = new GildedRose([new GildedRoseItem({ name: 'Normal Item', sellIn: 4, quality: 60 })]);
        const [normalItem] = gildedRose.updateQuality();
        expect(normalItem.quality).to.equal(50);
        expect(normalItem.sellIn).to.equal(3);
    });

})

describe('Update Quality, Aged Brie Items', () => {

    const itemName = GildedRoseItemName.AgedBrie;

    it('The Quality of an item is never more than 50', () => {
        const gildedRose = new GildedRose([new GildedRoseItem({ name: itemName, sellIn: 1, quality: 50 })]);
        const [normalItem] = gildedRose.updateQuality();
        expect(normalItem.quality).to.equal(50);
        expect(normalItem.sellIn).to.equal(0);
    });

    it('Should update Quality of Aged Brie, increase the quality', () => {
        const gildedRose = new GildedRose([new GildedRoseItem({ name: itemName, sellIn: 1, quality: 1 })]);
        const [normalItem] = gildedRose.updateQuality();
        expect(normalItem.quality).to.equal(2);
        expect(normalItem.sellIn).to.equal(0);
    });

    it('Quality increases by 2 when sellIn less than 0', () => {
        const gildedRose = new GildedRose([new GildedRoseItem({ name: itemName, sellIn: -5, quality: 2 })]);
        const items = gildedRose.updateQuality();
        const added = items[0]
        expect(added.quality).to.equal(4);
        expect(added.sellIn).to.equal(-6);
    });

    it('Quality increases by 2, Quality can not go up than 50', () => {
        const gildedRose = new GildedRose([new GildedRoseItem({ name: itemName, sellIn: -5, quality: 50 })]);
        const [normalItem] = gildedRose.updateQuality();
        expect(normalItem.quality).to.equal(50);
        expect(normalItem.sellIn).to.equal(-6);
    });

})

describe('Update Quality, Sulfuras Items', () => {

    const itemName = GildedRoseItemName.Sulfuras;

    it('should not sell and not decreases in Quality', () => {
        const gildedRose = new GildedRose([new GildedRoseItem({ name: itemName, sellIn: 0, quality: 80 })]);
        const [normalItem] = gildedRose.updateQuality();
        expect(normalItem.quality).to.equal(80);
        expect(normalItem.sellIn).to.equal(0);
    });
})

describe('Update Quality, Backstage passes Items', () => {

    const itemName = GildedRoseItemName.Backstage;

    it('The Quality of an item is never more than 50', () => {
        const gildedRose = new GildedRose([new GildedRoseItem({ name: itemName, sellIn: 1, quality: 50 })]);
        const [normalItem] = gildedRose.updateQuality();
        expect(normalItem.quality).to.equal(50);
        expect(normalItem.sellIn).to.equal(0);
    });

    it('Increase quality by 1 when sellIn more than 10 days', () => {
        const gildedRose = new GildedRose([new GildedRoseItem({ name: itemName, sellIn: 16, quality: 5 })]);
        const [normalItem] = gildedRose.updateQuality();
        expect(normalItem.quality).to.equal(6);
        expect(normalItem.sellIn).to.equal(15);
    });

    it('Quality increases by 2 when sellIn more than 5 days', () => {
        const gildedRose = new GildedRose([new GildedRoseItem({ name: itemName, sellIn: 6, quality: 1 })]);
        const [normalItem] = gildedRose.updateQuality();
        expect(normalItem.quality).to.equal(3);
        expect(normalItem.sellIn).to.equal(5);
    });

    it('Quality increases by 3 when sellIn less than 5 days', () => {
        const gildedRose = new GildedRose([new GildedRoseItem({ name: itemName, sellIn: 3, quality: 1 })]);
        const [normalItem] = gildedRose.updateQuality();
        expect(normalItem.quality).to.equal(4);
        expect(normalItem.sellIn).to.equal(2);
    });

    it('Quality will be 0 after concert', () => {
        const gildedRose = new GildedRose([new GildedRoseItem({ name: itemName, sellIn: 0, quality: 10 })]);
        const [normalItem] = gildedRose.updateQuality();
        expect(normalItem.quality).to.equal(0);
        expect(normalItem.sellIn).to.equal(-1);
    });
})

describe('Update Quality, Conjured Items', () => {

    const itemName = GildedRoseItemName.Conjured;

    it('Quality degrades twice as fast as Normal items', () => {
        const gildedRose = new GildedRose([new GildedRoseItem({ name: itemName, sellIn: 1, quality: 4 })]);
        const [normalItem] = gildedRose.updateQuality();
        expect(normalItem.quality).to.equal(2);
        expect(normalItem.sellIn).to.equal(0);
    });

    it('Quality degrades twice as fast as Normal items, SellIn less than zero', () => {
        const gildedRose = new GildedRose([new GildedRoseItem({ name: itemName, sellIn: -1, quality: 4 })]);
        const [normalItem] = gildedRose.updateQuality();
        expect(normalItem.quality).to.equal(0);
        expect(normalItem.sellIn).to.equal(-2);
    });

    it('The Quality of an item is never negative', () => {
        const gildedRose = new GildedRose([new GildedRoseItem({ name: itemName, sellIn: 0, quality: 1 })]);
        const [normalItem] = gildedRose.updateQuality();
        expect(normalItem.quality).to.equal(0);
        expect(normalItem.sellIn).to.equal(-1);
    });

})
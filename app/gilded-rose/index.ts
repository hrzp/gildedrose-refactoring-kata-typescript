import { GildedRoseItem, GildedRoseItemName } from "./models";
import { GildedRoseService } from "./services";

export class GildedRose {
    items: Array<GildedRoseItem>;
    protected gildedRoseService: GildedRoseService;
    protected handler;

    constructor(items = [] as Array<GildedRoseItem>) {
        this.items = items;
        this.gildedRoseService = new GildedRoseService();

        // Map items with functions
        this.handler = {
            [GildedRoseItemName.AgedBrie]: this.gildedRoseService.updateAgedBrie,
            [GildedRoseItemName.Backstage]: this.gildedRoseService.updateBackstage,
            [GildedRoseItemName.Sulfuras]: this.gildedRoseService.updateSulfuras,
            [GildedRoseItemName.Conjured]: this.gildedRoseService.updateConjured
        }
    }

    updateQuality() {
        this.items.forEach(item => {
            if (this.handler[item.name]) {
                item = this.handler[item.name](item);
            }
            else {
                item = this.gildedRoseService.updateNormalItem(item);
            }
        })
        return this.items;
    }
}

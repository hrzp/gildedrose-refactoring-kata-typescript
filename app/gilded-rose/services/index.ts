import { LEGEND_QUALITY, MAXIMUM_QUALITY, MINIMUM_QUALITY, SELL_IN_EXPIRE_DAY } from "../consts";
import { GildedRoseItem } from "../models";

export class GildedRoseService {

    /**
     * Update AgedBrie Quality and SellIn
     * Increases in Quality the older it gets
     * @param item GildedRoseItem
     * @returns GildedRoseItem
     */
    public updateAgedBrie = (item: GildedRoseItem): GildedRoseItem => {
        item.quality = this.increaseQuality(item.quality);

        // Increases if SellIn expired
        item.quality = this.isSellInExpired(item.sellIn) ? this.increaseQuality(item.quality) : item.quality
        item.sellIn -= 1;

        return item;
    }

    /**
     * Update Sulfuras
     * This is a legend item
     * @param item GildedRoseItem
     * @returns GildedRoseItem
    */
    public updateSulfuras = (item: GildedRoseItem): GildedRoseItem => {
        item.quality = LEGEND_QUALITY;
        return item;
    }

    /**
     * Update Normal items Quality and SellIn
     * Decreases in Quality the older it gets
     * @param item GildedRoseItem
     * @returns GildedRoseItem
     */
    public updateNormalItem = (item: GildedRoseItem): GildedRoseItem => {
        item.quality = this.decreaseQuality(item.quality);

        // Decrease if SellIn expired
        item.quality = this.isSellInExpired(item.sellIn) ? this.decreaseQuality(item.quality) : item.quality;
        item.sellIn -= 1;

        return item;
    }

    /**
     * Update Backstage items Quality and SellIn
     * Quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less but
       Quality drops to 0 after the concert
     * @param item GildedRoseItem
     * @returns GildedRoseItem
     */
    public updateBackstage = (item: GildedRoseItem): GildedRoseItem => {
        item.quality = item.sellIn === 0 ? 0 : this.increaseQualityForConcert(item);
        item.sellIn -= 1

        return item;
    }

    /**
     * Update Conjured items Quality and SellIn
     * "Conjured" items degrade in Quality twice as fast as normal items
     * @param item GildedRoseItem
     * @returns GildedRoseItem
     */
    public updateConjured = (item: GildedRoseItem): GildedRoseItem => {
        // quality should decrease x2
        let quality = item.quality >= 2 ? item.quality - 1 : item.quality;
        item.quality = this.decreaseQuality(quality);

        // Decrease if SellIn expired
        quality = item.quality >= 2 ? item.quality - 1 : item.quality;
        item.quality = this.isSellInExpired(item.sellIn) ? this.decreaseQuality(quality) : item.quality;

        item.sellIn -= 1

        return item;
    }

    private allowQualityToIncrease = (quality: number) => quality < MAXIMUM_QUALITY;
    private allowQualityToDecrease = (quality: number) => quality > MINIMUM_QUALITY;

    private isSellInExpired = (sellIn: number) => sellIn < SELL_IN_EXPIRE_DAY;

    private increaseQuality = (quality: number) => this.allowQualityToIncrease(quality) ? quality + 1 : quality;

    private decreaseQuality = (quality: number) => this.allowQualityToDecrease(quality) ? quality - 1 : quality;

    private increaseQualityForConcert = (item: GildedRoseItem): number => {
        let quality = this.increaseQuality(item.quality);
        // TODO: add more test cases
        quality = item.sellIn <= 10 ? this.increaseQuality(quality) : quality;
        quality = item.sellIn <= 5 ? this.increaseQuality(quality) : quality;

        return quality;
    }
}

export class GildedRoseItem {
    name: string;
    sellIn: number;
    quality: number;

    constructor({ name, sellIn, quality }) {
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
    }
}

export enum GildedRoseItemName {
    AgedBrie = 'Aged Brie',
    Sulfuras = 'Sulfuras, Hand of Ragnaros',
    Backstage = 'Backstage passes to a TAFKAL80ETC concert',
    Conjured = 'Conjured'
}
